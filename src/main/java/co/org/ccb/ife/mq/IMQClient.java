package co.org.ccb.ife.mq;

import java.util.ArrayList;

import co.org.ccb.ife.mq.model.MMessages;

public interface IMQClient {
	
	public void publish(String message);
	public ArrayList<MMessages> subscribe();
	public void connect();

}
