package co.org.ccb.ife.mq.model;

public class MMessages {

	private String messageId;

	private String receiptHandle;

	private String mD5OfBody;

	private String body;

	public MMessages(String messageId, String receiptHandle, String mD5OfBody, String body) {
		super();
		this.messageId = messageId;
		this.receiptHandle = receiptHandle;
		this.mD5OfBody = mD5OfBody;
		this.body = body;
	}

	public MMessages() {
		super();
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getReceiptHandle() {
		return receiptHandle;
	}

	public void setReceiptHandle(String receiptHandle) {
		this.receiptHandle = receiptHandle;
	}

	public String getmD5OfBody() {
		return mD5OfBody;
	}

	public void setmD5OfBody(String mD5OfBody) {
		this.mD5OfBody = mD5OfBody;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
