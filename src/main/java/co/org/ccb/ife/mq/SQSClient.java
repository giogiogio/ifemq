package co.org.ccb.ife.mq;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import co.org.ccb.ife.mq.model.MEntry;
import co.org.ccb.ife.mq.model.MMessages;

public class SQSClient implements IMQClient {

	private MEntry entry;
	private AmazonSQS sqs;
	private final static Logger LOGGER = Logger.getLogger("SQSClient.Control");

	public SQSClient(MEntry entry) {
		this.entry = entry;
	}

	public void publish(String message) {
		try {
			SendMessageRequest sendMessageRequest = new SendMessageRequest();
			sendMessageRequest.withMessageBody(message);
			sendMessageRequest.withQueueUrl(entry.getUrl());
			sqs.sendMessage(sendMessageRequest);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
	}

	public ArrayList<MMessages> subscribe() {
		try {
			ArrayList<MMessages> response = new ArrayList<MMessages>();
			List<Message> messages = sqs.receiveMessage(entry.getUrl()).getMessages();
			for (Message m : messages) {
				response.add(new MMessages(m.getMessageId(), m.getReceiptHandle(), m.getMD5OfBody(), m.getBody()));
				sqs.deleteMessage(entry.getUrl(), m.getReceiptHandle());
			}
			return response;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
		return null;
	}

	public void connect() {
		try {
			AWSCredentialsProvider awsCredentials2 = new AWSStaticCredentialsProvider(
					new BasicAWSCredentials(entry.getAccessKey(), entry.getSecretKey()));
			sqs = AmazonSQSClientBuilder.standard().withRegion(entry.getRegion()).withCredentials(awsCredentials2)
					.build();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
		}
	}

}
