package co.org.ccb.ife.mq.model;

public class MEntry {

	private String accessKey;
	private String secretKey;
	private String url;
	private String region;

	public MEntry() {
		super();
	}

	public MEntry(String accessKey, String secretKey, String url, String region) {
		super();
		this.accessKey = accessKey;
		this.secretKey = secretKey;
		this.url = url;
		this.region = region;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

}
