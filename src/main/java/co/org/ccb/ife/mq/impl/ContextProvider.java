package co.org.ccb.ife.mq.impl;

import java.util.ArrayList;

import co.org.ccb.ife.mq.IMQClient;
import co.org.ccb.ife.mq.model.MMessages;

public class ContextProvider {
	
	private IMQClient iMQClient;
	
	public ContextProvider(IMQClient iMQClient) {
		this.iMQClient=iMQClient;
	}
	
	public void publish(String message) {
		this.iMQClient.publish(message);
	}
	public ArrayList<MMessages> subscribe() {
		return this.iMQClient.subscribe();
	}
	public void connect() {
		this.iMQClient.connect();
	}

}
